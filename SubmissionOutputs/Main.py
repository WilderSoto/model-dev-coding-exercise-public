#!/usr/bin/env python
# coding: utf-8

# In[138]:


import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
from datetime import date, timedelta


# ## Model Development Programming Exercise
# 
# #### Task 1: Reading in the Data

# In[43]:


#Combining the fuel prices to one dataframe object
fuelprice = pd.concat([pd.read_csv("GDA_TETSTX.csv"), pd.read_csv("Henry_Hub.csv")], axis=0,ignore_index=True)

#Combining the power prices into one dataframe
power = [pd.read_csv(f"ERCOT_DA_Prices_20{str(i)}.csv") for i in range(16,20)]
powerprice = pd.concat(power,axis = 0,ignore_index=True)

#Reading in the contracts and plant parameters

contracts = pd.read_csv("Contracts.csv")
plantparam = pd.read_csv("Plant_Parameters.csv")


# ### Power Price Statistics
# 
# #### Task 2: Calculate basic descriptive statistics

# In[106]:


#Compute the mean, min, max, and standard deviations of the hourly prices for each settlement point and year-month

def PowerStats(df):
    d = df.copy(deep = True) #Making a copy so I don't ruin the original variable
    d.Date = [i[:7] for i in df.Date] #Only grabbing the year and month
    
    means = d.groupby(['Date','SettlementPoint'],as_index=False)['Price'].mean() #Grouping the data frame then applying an operation
    maxes = d.groupby(['Date','SettlementPoint'],as_index=False)['Price'].max()
    mins = d.groupby(['Date','SettlementPoint'],as_index=False)['Price'].min()
    sd = d.groupby(['Date','SettlementPoint'],as_index=False)['Price'].std()
    df2 = pd.DataFrame({'Date': means['Date'],'SettlementPoint': means['SettlementPoint'],'Mean': means['Price'],'Max': maxes['Price'],'Min': mins['Price'],'SD': sd['Price']})
    #Combining into one data frame
    return df2 # Returning the dataframe

stats = PowerStats(powerprice)


# #### Task 3: Calculate Volatility

# In[113]:


#Calculating the volatility

def PowerVolatility(df):
    
    vals = []
    SPnames = []
    time = []
    for SP in df.SettlementPoint.unique():
        
        d = df[df['SettlementPoint'] == SP].copy(deep = True)
        d.Date = [i[:7] for i in d.Date]
        v = []
        
        for mth in d.Date.unique():
            logs = []
            p = d[d['Date'] == mth]['Price'].to_list()
            for i in range(1,len(p)):
                if p[i-1] == 0 and p[i] != 0:
                    logs += [1e256]
                elif p[i-1] != 0 and p[i] == 0:
                    logs += [-1e256]
                elif p[i-1] == p[i]:
                    logs += [0]
                else:
                    logs += [np.log(p[i]/p[i-1])]
            time += [mth]
            v += [np.std(logs)*math.sqrt(12)] #Monthly volatility, based on formula found on google

        vals += v
        SPnames += len(v)*[SP]
        
    return pd.DataFrame({'Date': time,'SettlementPoint':SPnames,'Volatility':vals})
    
volatility = PowerVolatility(powerprice)


# #### Task 4: Write results to file

# In[131]:


comb = stats.merge(volatility, on=['Date', 'SettlementPoint'])
comb['Year'] = [i[:4] for i in comb.Date]
comb['Month'] = [i[6:] for i in comb.Date]
comb = comb[["SettlementPoint", "Year", "Month", "Mean", "Min", "Max", "SD", "Volatility"]]
comb.to_csv('MonthlyPowerPriceStatistics.csv')


# In[91]:


#Note we have some missing data: Not all of the settlement points are represented in all of the months


# ### Contract Valuations
# #### Task 5: Expand the contracts across relevant time periods

# In[143]:


sdateD_HH = date(2017,1,1)   # start date
edateD_HH = date(2017,3,31)   # end date
sdateD_GDA = date(2017,6,1)   # start date
edateD_GDA = date(2017,7,15)   # end date
sdateH_HBN = date(2017,1,1)   # start date
edateH_HBN = date(2018,12,31)   # end date
sdateH_HBH = date(2018,10,1)   # start date
edateH_HBH = date(2019,9,30)   # end date

HH_dates = pd.date_range(sdateD_HH,edateD_HH,freq='d')
GDA_dates = pd.date_range(sdateD_GDA,edateD_GDA,freq='d')
HBN_dates = pd.date_range(sdateH_HBN,edateH_HBN,freq='h')
HBH_dates = pd.date_range(sdateH_HBH,edateH_HBH,freq='h')

HH = pd.DataFrame({'Date': HH_dates,'ContractName': len(HH_dates)*['S1'],
                   'DealType':len(HH_dates)*['Swap'],'Volume': len(HH_dates)*[20000],'StrikePrice': len(HH_dates)*[3.0],
                  'Premium': len(HH_dates)*[np.nan],'PriceName':len(HH_dates)*['Henry Hub']})
GDA = pd.DataFrame({'Date': GDA_dates,'ContractName': len(GDA_dates)*['O1'],
                   'DealType':len(GDA_dates)*['European Option'],'Volume': len(GDA_dates)*[10000],'StrikePrice': len(GDA_dates)*[2.9],
                  'Premium': len(GDA_dates)*[0.1],'PriceName':len(GDA_dates)*['GDA_TETSTX']})

HBN = pd.DataFrame({'Date': HBN_dates,'ContractName': len(HBN_dates)*['S2'],
                   'DealType':len(HBN_dates)*['Swap'],'Volume': len(HBN_dates)*[1000],'StrikePrice': len(HBN_dates)*[21.0],
                  'Premium': len(HBN_dates)*[np.nan],'PriceName':len(HBN_dates)*['HB_NORTH']})
HBH = pd.DataFrame({'Date': HBH_dates,'ContractName': len(HBH_dates)*['O2'],
                   'DealType':len(HBH_dates)*['European Option'],'Volume': len(HBH_dates)*[1000],'StrikePrice': len(HBH_dates)*[31.5],
                  'Premium': len(HBH_dates)*[3.0],'PriceName':len(HBH_dates)*['HB_HOUSTON']})

Daily = pd.concat([HH,GDA],axis = 0,ignore_index = True)
Hourly = pd.concat([HBN,HBH],axis = 0,ignore_index = True)


# #### Task 6: Join relevant prices
# 
# First to do the fuel prices

# In[165]:


fp = fuelprice.copy()
fp = fp.rename({'Variable': 'PriceName'}, axis=1)
Daily.Date = Daily.Date.astype(str)
dal = Daily.merge(fp, on=['Date','PriceName'])


# Now to do the power prices

# In[166]:


pp = powerprice.copy()
pp = pp.rename({'SettlementPoint': 'PriceName'}, axis=1)
Hourly.Date = Hourly.Date.astype(str)
hour = Hourly.merge(pp, on=['Date','PriceName'])


# #### Task 7: Calculate payoffs
# 
# Hourly first

# In[168]:


hour_s_p = hour[hour['DealType'] == 'Swap']['Price'].to_list()
hour_s_strp = hour[hour['DealType'] == 'Swap']['StrikePrice'].to_list()
payoff_hs = []
for i in range(len(hour_s_p)):
    payoff_hs += [1000*(hour_s_p[i]-hour_s_strp[i])]
    
    
hour_eo_p = hour[hour['DealType'] == 'European Option']['Price'].to_list()
hour_eo_strp = hour[hour['DealType'] == 'European Option']['StrikePrice'].to_list()
hour_eo_prem = hour[hour['DealType'] == 'European Option']['Premium'].to_list()
payoff_heo = []
for i in range(len(hour_eo_p)):
    payoff_heo += [1000*(max(hour_eo_p[i]-hour_eo_strp[i],0)-hour_eo_prem[i])]
    
H_EO = hour[hour['DealType'] == 'European Option']
H_EO['Payoff'] = payoff_heo
H_S = hour[hour['DealType'] == 'Swap']
H_S['Payoff'] = payoff_hs
hour = pd.concat([H_S,H_EO],axis = 0)


# Now for daily

# In[169]:


dal_s_p = dal[dal['DealType'] == 'Swap']['Price'].to_list()
dal_s_strp = dal[dal['DealType'] == 'Swap']['StrikePrice'].to_list()
payoff_ds = []
for i in range(len(dal_s_p)):
    payoff_ds += [1000*(dal_s_p[i]-dal_s_strp[i])]
    
    
dal_eo_p = dal[dal['DealType'] == 'European Option']['Price'].to_list()
dal_eo_strp = dal[dal['DealType'] == 'European Option']['StrikePrice'].to_list()
dal_eo_prem = dal[dal['DealType'] == 'European Option']['Premium'].to_list()
payoff_deo = []
for i in range(len(dal_eo_p)):
    payoff_deo += [1000*(max(dal_eo_p[i]-dal_eo_strp[i],0)-dal_eo_prem[i])]
    
D_EO = dal[dal['DealType'] == 'European Option']
D_EO['Payoff'] = payoff_deo
D_S = dal[dal['DealType'] == 'Swap']
D_S['Payoff'] = payoff_ds
dal = pd.concat([D_S,D_EO],axis = 0)


# In[ ]:





# #### Task 8+9: Calculate aggregated payoffs and Write the results to file

# In[184]:


hour['Year'] = [i[:4] for i in hour.Date]
dal['Year'] = [i[:4] for i in dal.Date]
hour['Month'] = [i[5:7] for i in hour.Date]
dal['Month'] = [i[5:7] for i in dal.Date]

dal2 = dal.groupby(['ContractName','Year','Month','PriceName'])['Payoff'].sum().reset_index()
hour2 = hour.groupby(['ContractName','Year','Month','PriceName'])['Payoff'].sum().reset_index()
dal2 = dal2.rename({'Payoff': 'TotalPayoff'}, axis=1)
hour2 = hour2.rename({'Payoff': 'TotalPayoff'}, axis=1)

pays = pd.concat([dal2,hour2],axis = 0)

pays = pays[['ContractName', 'Year','Month', 'TotalPayoff']]
pays.to_csv('MonthlyContractPayoffs.csv')


# ### Plant Dispath Modeling
# 
# #### Task 10: Calculate the hourly running cost of each power plant included in the Plant_Parameters.csv input file

# In[ ]:


The formula for running cost (in $/MWh) is:
▪ RunningCost = ((FuelPrice + FuelTransportationCost) x HeatRate) + VOM
o Note that the FuelTransportationCost, HeatRate, and VOM parameters are

provided in Plant_Parameters and are assumed to be constant for each plant-
year-month. Fuel prices, however, change on a daily basis. Names of the

corresponding fuel prices for each plant are provided in the FuelPriceName
column, and daily prices must be joined from the data provided in the
’fuelPrices’ input folder.
o The result of this calculation should be a data frame that contains data for 2
plants x 2 years (2017 and 2018) x 365 days per year = 1460 total rows


# In[223]:


fp2 = fp.copy()
fp2['Year'] = [i[:4] for i in fp2.Date]
fp2['Month'] = [i[5:7] for i in fp2.Date]
#fp2 = fp2.rename({'PriceName':"FuelPriceName"})
fpr_Alpha = fp2[(fp2['PriceName'] == 'Henry Hub')]
fpr_Alpha = fpr_Alpha[(fpr_Alpha['Year'] == '2017') | (fpr_Alpha['Year'] == '2018')].reset_index()
fpr_Bravo = fp2[(fp2['PriceName'] == 'GDA_TETSTX')]
fpr_Bravo = fpr_Bravo[(fpr_Bravo['Year'] == '2017') | (fpr_Bravo['Year'] == '2018')].reset_index()


# In[224]:





# In[244]:


costs = []
yA = []
mA = []
station = []
for i in range(len(fpr_Alpha.Price)):
    vals = plantparam[(plantparam['PlantName'] == 'Alpha')&(plantparam['Month'] == int(fpr_Alpha.Month[i])) & (plantparam['Year'] == int(fpr_Alpha.Year[i]))].reset_index()
    vals
    cost = (fpr_Alpha.Price[i]+vals.FuelTransportationCost[0])*vals.HeatRate[0]+vals.VOM[0]
    costs.append(cost)
    yA += [fpr_Alpha.Year[i]]
    mA += [fpr_Alpha.Month[i]]
    station += ['Alpha']
    
for i in range(len(fpr_Bravo.Price)):
    vals = plantparam[(plantparam['PlantName'] == 'Bravo')&(plantparam['Month'] == int(fpr_Bravo.Month[i])) & (plantparam['Year'] == int(fpr_Bravo.Year[i]))].reset_index()
    
    cost = (fpr_Bravo.Price[i]+vals.FuelTransportationCost[0])*vals.HeatRate[0]+vals.VOM[0]
    costs.append(cost)
    yA += [fpr_Bravo.Year[i]]
    mA += [fpr_Bravo.Month[i]]
    station += ['Bravo']
    
fuelcosts = pd.DataFrame({'RunningCosts':costs,'PlantName':station,'Year':yA,'Month':mA})
    
fuelcosts.to_csv('Task10.csv')


# In[242]:


vals


# In[ ]:




